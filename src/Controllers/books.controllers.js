const servicesInstance = require("../Services/books.services");

const getBookById = async (req, res) => {
  let id = req.params.id;
  try {
    let result = await servicesInstance.getBooksByIdService(id);
    if (!result) {
      throw new Error("Book not found");
    }
    res.status(200).send(result);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

const getAllBooks = async (req, res) => {
  let result = await servicesInstance.getAllBooksService(req.query);
  if (result) {
    res.status(200).send(result);
  } else {
    res.status(400).send(result);
  }
};

const getBooksByAuthorName = async (req, res) => {
  try {
    let result = await servicesInstance.getBooksByAuthorNameService(
      req.params.authorSubstring
    );
    res.status(200).send(result);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

const getBooksByTitle = async (req, res) => {
  try {
    let result = await servicesInstance.getBooksBytitleService(
      req.params.titleSubstring
    );
    res.status(200).send(result);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

const getBooksByPriceRange=async(req,res)=>{
  let start=req.query.start;
  let end=req.query.end;
  try{
    let results=await servicesInstance.getBooksByPriceRangeService(start,end);
    res.status(200).send(results)
  }catch(err){
    res.status(400).send(err)
  }
}

const createNewBook = async (req, res) => {
  try {
    let result = await servicesInstance.createNewBookService(req.body);
    res.status(200).send(`Book added with title ${req.body.title}`);
  } catch (err) {
    res.status(400).send(err.errors[0].message);
  }
};

const deleteBookById = async (req, res) => {
  try {
    let result = await servicesInstance.deleteBookService(req.params.id);
    if (!result) {
      throw new Error("Book not found");
    }
    res.status(200).send(`Book Deleted with Id ${req.params.id}`);
  } catch (err) {
    res.status(400).send(err.message);
  }
};

const updateBookById = async (req, res) => {
  let id = req.params.id;
  try {
    let result = await servicesInstance.updateBookService(id, req.body);
    if (!result) {
      throw new Error("Book not found");
    }
    res.status(200).send(" Book Updated");
  } catch (err) {
    res.status(400).send(err.message);
  }
};

module.exports = {
  getBookById,
  getAllBooks,
  createNewBook,
  updateBookById,
  deleteBookById,
  getBooksByAuthorName,
  getBooksByTitle,
  getBooksByPriceRange
};
