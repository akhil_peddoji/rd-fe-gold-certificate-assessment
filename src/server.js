const express = require("express");
const db = require("./models/books.model");
const booksRoute = require("./Routes/books.routes");
const app = express();
const port = 3000;

app.use(express.json());
app.use("/api/", booksRoute);
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});

app.use((err,req,res,next)=>{
    res.status(err.status),
    res.json({
        error:{
            status:err.status,
            message:err.message
        }
    })
});

db.sequelize
  .sync()
  .then(() => {
    console.log("Connected to db");
  })
  .catch((err) => {
    console.log("error occured while connecting to db ", err.message);
  });
