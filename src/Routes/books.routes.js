const express = require("express");
const booksController = require("../Controllers/books.controllers");
const bookMiddleware = require("../middlewares/books.middleware");
const router = express.Router();

//  GET /books: Retrieve a list of all books.
// for pagination books?page=1&limit=2&sortBy=title&order=ASC
router.get("/books", booksController.getAllBooks);

// get books by author (substring matching)
router.get(
  "/books/author/:authorSubstring",
  booksController.getBooksByAuthorName
);

// get books by title (substring matching)
router.get("/books/title/:titleSubstring", booksController.getBooksByTitle);

// get books in a price range sample(http://localhost:3000/api/books/price?start=50&end=345)
router.get("/books/price", booksController.getBooksByPriceRange);

// GET /books/{id}: Retrieve the details of a specific book by its ID.
router.get("/books/:id", booksController.getBookById);

// POST /books: Create a new book.
router.post(
  "/books",
  bookMiddleware.validateBook,
  booksController.createNewBook
);

// PUT /books/{id}: Update the details of a specific book by its ID.
router.patch(
  "/books/:id",
  bookMiddleware.validateUpdateBook,
  booksController.updateBookById
);

// DELETE /books/{id}: Delete a specific book by its ID.
router.delete("/books/:id", booksController.deleteBookById);

module.exports = router;
