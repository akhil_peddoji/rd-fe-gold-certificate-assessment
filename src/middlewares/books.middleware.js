const joi = require("joi");
const createHttpError = require("http-errors");

let bookJoiModel = joi.object({
  title: joi.string().required(),
  author: joi.string().required(),
  price: joi.number().required(),
});

let updateBookModel = joi.object({
  title: joi.string(),
  author: joi.string(),
  price: joi.number(),
});

const validateBook = (req, res, next) => {
  const val = bookJoiModel.validate(req.body);
  console.log(req.body, " validate middleware");
  if (val.error) {
    return next(createHttpError(400, val.error));
  }
  next();
};

const validateUpdateBook = (req, res, next) => {
  let val = updateBookModel.validate(req.body);
  if (val.error) {
    return next(createHttpError(400, val.error));
  }
  next();
};

module.exports = { validateBook, validateUpdateBook };
