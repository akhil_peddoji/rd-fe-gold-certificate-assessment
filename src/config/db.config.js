module.exports = {
  HOST: "localhost",
  USER: "postgres",
  PASSWORD: "deku123",
  DB: "Books",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};