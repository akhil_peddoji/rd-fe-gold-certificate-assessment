const dbInsance = require("../Db/books.db");
const uuid = require("uuid");

const getAllBooksService = async (query) => {
  let page = query.page ? query.page : 0;
  let limit = query.limit ? query.limit : 0;
  let sortBy = query.sortBy ? query.sortBy : "price";
  let order = query.order ? query.order : "ASC";
  if (page > 0 && limit > 0) {
    return await dbInsance.getAllBooksPaginateDb(page, limit, sortBy, order);
  }
  return await dbInsance.getAllBooksDb();
};

const getBooksByIdService = async (id) => {
  return await dbInsance.getBooksByIdDb(id);
};

const getBooksByAuthorNameService = async (author) => {
  return await dbInsance.getBooksByAuthorDb(author);
};

const getBooksBytitleService = async (title) => {
  return await dbInsance.getBooksByTitleDb(title);
};

const getBooksByPriceRangeService = async (start, end) => {
  return await dbInsance.getBooksByPriceRangeDb(start, end);
};

const createNewBookService = async (bookData) => {
  let newBook = {
    id: uuid.v4().substring(0, 8),
    ...bookData,
  };
  let result = await dbInsance.createNewBookDb(newBook);
  return result;
};

const updateBookService = async (id, bookData) => {
  let result = await dbInsance.updateBookByIdDb(id, bookData);
  return result;
};

const deleteBookService = async (id) => {
  let result = await dbInsance.deleteBookByIdDb(id);
  return result;
};

module.exports = {
  getAllBooksService,
  getBooksByIdService,
  createNewBookService,
  updateBookService,
  deleteBookService,
  getBooksByAuthorNameService,
  getBooksBytitleService,
  getBooksByPriceRangeService,
};
