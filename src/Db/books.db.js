const { booksPgSchema } = require("../models/books.model");
const { Op } = require("sequelize");
// to get all Books

const getAllBooksDb = async () => {
  let dbResults = await booksPgSchema.findAll();
  return dbResults;
};

const getAllBooksPaginateDb = async (page, limit,sortBy,order) => {
  let offset = (page - 1) * limit;
  let dbResults = await booksPgSchema.findAndCountAll({
    limit: limit,
    offset: offset,
    order:[[sortBy,order]]
  });
  return dbResults;
};


const getBooksByIdDb = async (id) => {
  let dbResults = await booksPgSchema.findOne({
    where: { id },
  });
  return dbResults;
};

const getBooksByAuthorDb = async (author) => {
  let dbResults = await booksPgSchema.findAll({
    where: {
      author: {
        [Op.substring]: author,
      },
    },
  });
  return dbResults;
};

const getBooksByTitleDb = async (title) => {
  let dbResults = await booksPgSchema.findAll({
    where: {
      title: {
        [Op.substring]: title,
      },
    },
  });
  return dbResults;
};

const getBooksByPriceRangeDb=async(start,end)=>{
  let dbResults=await booksPgSchema.findAll({
    where:{
      price:{
        [Op.gte]:start,
        [Op.lte]:end
      }
    }
  });
  return dbResults;
}

const createNewBookDb = async (newBook) => {
  let dbResult = await booksPgSchema.create(newBook);
  return dbResult;
};

const updateBookByIdDb = async (id, bookData) => {
  let dbResult = await booksPgSchema.update(bookData, {
    where: { id: id },
  });
  return dbResult;
};

const deleteBookByIdDb = async (id) => {
  let dbResult = await booksPgSchema.destroy({
    where: { id },
  });
  return dbResult;
};

module.exports = {
  getAllBooksDb,
  getBooksByIdDb,
  createNewBookDb,
  updateBookByIdDb,
  deleteBookByIdDb,
  getAllBooksPaginateDb,
  getBooksByAuthorDb,
  getBooksByTitleDb,
  getBooksByPriceRangeDb
};
